

const express = require('express');
const config = require('./config/config');
const glob = require('glob');
const mongoose = require('mongoose');
const modules = [];

mongoose.connect(config.db);
const db = mongoose.connection;
db.on('error', () => {
  throw new Error('unable to connect to database at ' + config.db);
});

var modulesPaths = glob.sync(config.root +  '/app/**/module.js');
	modulesPaths.forEach((modulePath) => {
		modules.push (require(modulePath));
	});	


//Iniciamos los modelos

modules.forEach ( (module) => {
	module.initModels();
});

const app = express();

module.exports = require('./config/express')(app, config, modules);

app.listen(config.port, () => {
  console.log('Express server listening on port ' + config.port);
});

