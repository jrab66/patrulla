const chai = require('chai');
const should = chai.should();

const inspectorService = require ('../services/inspector-service');

describe ('This is the inspector module tests', () => {
	describe ('Inspeccionará el aire de las ruedas', () => {
		it ('Rueda hinchada', () => {
			inspectorService.airLevel().should.be.at.least (2);
		});
	});
});