module.exports = function() {
	it('Inspector home page',function(){
		cy.visit('/inspector');
		cy.title().should('include','Patrulla Inspector');		
	});
	it('Inspector body',function(){
		cy.get('h1').should('contain','Patrulla Inspector');
	});
}
