const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const inspectorService = require('../services/inspector-service');

module.exports = (app) => {
  app.use('/inspector', router);
};

router.get('/', (req, res, next) => {

	const airLevel = inspectorService.airLevel();
	res.render('index', {
		title: 'Patrulla Inspector',
		airLevel: airLevel
	});
});
